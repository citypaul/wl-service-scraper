FROM node:6.6.0

RUN mkdir -p /home/nodejs/app  
WORKDIR /home/nodejs/app

COPY . /home/nodejs/app  
RUN npm install

EXPOSE 8080
CMD ["npm", "start"] 
