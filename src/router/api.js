import express from 'express';
import fakeModel from '../fakes/fake-model';

const router = express.Router(); // eslint-disable-line new-cap

router.get('/', (req, res) => {
  res.json(fakeModel);
});

router.get('/:id', (req, res) => {
  res.json(fakeModel.response.filter(model => model.id === parseInt(req.params.id, 10)));
});

export default router;
