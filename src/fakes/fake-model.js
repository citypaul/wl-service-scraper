const fakeModel = (id, title, description, price) => ({ id, title, description, price });

export default {
  response: [
    fakeModel(1, 'My Product', 'My description', '5.36'),
    fakeModel(2, 'My Product 2', 'My description 2', '0.36'),
  ],
};
