import express from 'express';
import bodyParser from 'body-parser';
import apiRouter from './router/api';

const app = express();
const port = process.env.PORT || 8080;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/api', apiRouter);
app.listen(port);

console.log(`Listening on port ${port}`); // eslint-disable-line no-console
